<?php
/**
 * Palindrome Tests
 *
 * @package Slacademic
 * @subpackage Tests
 * @uathor Eric
 */
namespace Slacademic\Tests\Questions;

class PalindromeTest extends \PHPUnit\Framework\TestCase
{
    public function testQuestion()
    {
        $this->assertTrue(\Slacademic\Questions\Palindrome::isPalindrome('taco cat'));
        $this->assertFalse(\Slacademic\Questions\Palindrome::isPalindrome('burrito dog'));
    }
}