<?php

namespace Slacademic\Tests\Questions;

class NwSeMatrixTest extends \PHPUnit\Framework\TestCase
{
    public function testNwSeMatrixPrint()
    {
        $matrix = new \Slacademic\Questions\NwSeMatrix();
        $matrix->printMatrix([
            [1, 2, 3, 4],
            [5, 6, 7, 8],
            [9, 10, 11, 12]
        ]);

        $this->expectOutputString("9 \n5 10 \n1 6 11 \n2 7 12 \n3 8 \n4 \n");
    }
}