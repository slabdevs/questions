<?php
/**
 * Question Test
 *
 * @package Slacademic
 * @subpackage Tests
 * @uathor Eric
 */
namespace Slacademic\Tests\Questions;

class LinkedListCycleTest extends \PHPUnit\Framework\TestCase
{
    public function testQuestion()
    {
        $node1 = new \Slacademic\Questions\LinkedListCycle(1);
        $node2 = new \Slacademic\Questions\LinkedListCycle(2);
        $node3 = new \Slacademic\Questions\LinkedListCycle(3);
        $node4 = new \Slacademic\Questions\LinkedListCycle(4);

        $node1->next = $node2;
        $node2->next = $node3;
        $node3->next = $node4;

        $this->assertNull(\Slacademic\Questions\LinkedListCycle::findCycle($node1));

        $node4->next = $node3;

        $this->assertEquals($node3, \Slacademic\Questions\LinkedListCycle::findCycle($node1));

    }

}