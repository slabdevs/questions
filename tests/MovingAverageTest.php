<?php
/**
 * Tests
 *
 * @package Slacademic
 * @subpackage Tests
 * @uathor Eric
 */
namespace Slacademic\Tests\Questions;

class MovingAverageTest extends \PHPUnit\Framework\TestCase
{
    public function testQuestion()
    {
        $average = new \Slacademic\Questions\MovingAverage(5);

        $average->pushNumber(10);

        $this->assertEquals(10, $average->average());

        $average->pushNumber(20);
        $average->pushNumber(30);

        $this->assertEquals(20, $average->average());

        $average->pushNumber(33);
        $average->pushNumber(11);
        $average->pushNumber(22);
        $average->pushNumber(55);
        $average->pushNumber(77);
        $average->pushNumber(44);
        $average->pushNumber(89);
        $average->pushNumber(99);
        $average->pushNumber(13);
        $average->pushNumber(22);
        $average->pushNumber(14);

        $this->assertEquals(((14 + 22 + 13 + 99 + 89) / 5), $average->average());

    }
}