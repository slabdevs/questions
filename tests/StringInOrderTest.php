<?php
/**
 * @see https://www.careercup.com/question?id=5179225018466304
 */
namespace Slacademic\Tests\Questions;

class StringInOrderTest extends \PHPUnit\Framework\TestCase
{
    public function testQuestion()
    {
        $this->assertTrue(\Slacademic\Questions\StringInOrder::inOrder(['cc', 'cb', 'bb', 'ac'], ['c','b','a']));
        $this->assertFalse(\Slacademic\Questions\StringInOrder::inOrder(['cc', 'cb', 'bb', 'ac'], ['b','c','a']));
    }
}