<?php
/**
 * Test random string within string test
 *
 * @package Slacademic
 * @subpackage Tests
 * @author Eric
 */
namespace Slacademic\Tests\Questions;

class RandomStringWithinStringTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Test minimum substring
     */
    public function testMinimumSubstring()
    {
        $stringer = new \Slacademic\Questions\RandomStringWithinString();

        $value = $stringer->getMinimumSubString('adobecodebanc', 'abc');

        $this->assertEquals('banc', $value);
    }
}