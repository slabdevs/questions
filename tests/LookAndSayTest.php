<?php
/**
 * Look and Say Test
 *
 * @package Slacademic
 * @subpackage Tests
 * @uathor Eric
 */
namespace Slacademic\Tests\Questions;

class LookAndSayTest extends \PHPUnit\Framework\TestCase
{
    public function testQuestion()
    {
        $this->expectOutputString('1' . PHP_EOL);
        \Slacademic\Questions\LookAndSay::las(1);
    }

    public function testFail()
    {
        $this->expectOutputString('');
        \Slacademic\Questions\LookAndSay::las(0);
        \Slacademic\Questions\LookAndSay::las(-13);
    }

    public function testQuestion2()
    {
        $output = '1' . PHP_EOL;
        $output.= '11'. PHP_EOL;
        $output.= '21'. PHP_EOL;
        $output.= '1211' . PHP_EOL;
        $output.= '111221' . PHP_EOL;
        $output.= '312211' . PHP_EOL;
        $output.= '13112221' . PHP_EOL;
        $output.= '1113213211' . PHP_EOL;
        $output.= '31131211131221' . PHP_EOL;
        $output.= '13211311123113112211' . PHP_EOL;

        $this->expectOutputString($output);
        \Slacademic\Questions\LookAndSay::las(10);
    }
}