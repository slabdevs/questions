<?php
/**
 * Test for moving zeroes to end of array
 *
 * @package Slacademic
 * @subpackage Questions
 * @author Eric
 */
namespace Slacademic\Tests\Questions;

class InToEnglishTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Move zeroes to end test
     *
     * @param $array
     * @dataProvider dataProviderIntToEnglish
     */
    public function testMoveZerosToEnd($input, $expected)
    {
        $formatter = new \Slacademic\Questions\IntToEnglish();

        $output = $formatter->englishRepresentationOfInteger($input);

        $this->assertEquals($expected, $output);
    }

    /**
     * Data provider for test
     *
     * Generates 100 tests that have 50 random numbers where each number has 50% chance of being a zero
     *
     * @return array
     */
    public function dataProviderIntToEnglish()
    {
        return [
            ['74981', 'Seventy Four Thousand Nine Hundred Eighty One'],
            ['1234', 'One Thousand Two Hundred Thirty Four'],
            ['90', 'Ninety'],
            ['9874', 'Nine Thousand Eight Hundred Seventy Four'],
            ['123', 'One Hundred Twenty Three'],
            ['100', 'One Hundred'],
            ['1', 'One'],
            ['47', 'Fourty Seven'],
            ['2239817293', 'Two Billion Two Hundred Thirty Nine Million Eight Hundred Seventeen Thousand Two Hundred Ninety Three']
        ];
    }
}