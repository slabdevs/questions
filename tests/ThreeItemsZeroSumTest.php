<?php

namespace Slacademic\Tests\Questions;

class ThreeItemsZeroSumTest extends \PHPUnit\Framework\TestCase
{
    public function testExecute()
    {
        $items = \Slacademic\Questions\ThreeItemsZeroSum::execute([0, -1, 2, -3, 1]);
        $this->assertSame(
            [
                [0, -1, 1],
                [2, -3, 1],
            ],
            $items
        );
    }

    public function testExecute2()
    {
        $items = \Slacademic\Questions\ThreeItemsZeroSum::execute2([0, -1, 2, -3, 1]);
        $this->assertSame(
            [
                [-1, 0, 1],
                [-3, 2, 1],
            ],
            $items
        );
    }

}