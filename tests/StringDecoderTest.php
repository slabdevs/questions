<?php
/**
 * Test string decoder
 *
 * @package Slacademic
 * @subpackage Questions
 * @author Eric
 */
namespace Slacademic\Tests\Questions;

class StringDecoderTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Test string decoder
     *
     * @dataProvider dataProviderForStringDecoder
     */
    public function testStringDecoder($input, $output)
    {
        $stringDecoder = new \Slacademic\Questions\StringDecoder();

        $this->assertEquals($output, $stringDecoder->decode($input));
    }

    /**
     * Data provider for string decoder
     *
     * @return array
     */
    public function dataProviderForStringDecoder()
    {
        return [
            ['3[a2[bd]g4[ef]h]', 'abdbdgefefefefhabdbdgefefefefhabdbdgefefefefh'],
            ['2[c3[ab]]', 'cabababcababab']
        ];
    }


}