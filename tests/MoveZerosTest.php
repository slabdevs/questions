<?php
/**
 * Test for moving zeroes to end of array
 *
 * @package Slacademic
 * @subpackage Questions
 * @author Eric
 */
namespace Slacademic\Tests\Questions;

class MoveZerosTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Move zeroes to end test
     *
     * @param $array
     * @dataProvider dataProviderTestMoveZerosToEnd
     */
    public function testMoveZerosToEnd($array)
    {
        $mover = new \Slacademic\Questions\MoveZeros();

        $mover->moveZerosToEnd($array);

        $foundZero = false;
        for ($i=0;$i<count($array);++$i)
        {
            if ($array[$i] == 0)
            {
                $foundZero = true;
            }
            else if ($foundZero)
            {
                $this->fail('Found a non-zero value after we already found a zero in the array.');
            }
        }

        $this->addToAssertionCount(1);
    }

    /**
     * Data provider for test
     *
     * Generates 100 tests that have 50 random numbers where each number has 50% chance of being a zero
     *
     * @return array
     */
    public function dataProviderTestMoveZerosToEnd()
    {
        $output = [];
        mt_srand(time());

        for ($i=0; $i<100; ++$i)
        {
            $testSample = [];
            for ($j = 0; $j < 50; ++$j)
            {
                if (mt_rand(1,2) == 1)
                {
                    $testSample[] = 0;
                }
                else
                {
                    $testSample[] = mt_rand(1, 50);
                }
            }
            $output[] = [$testSample];
        }

        return $output;
    }

}