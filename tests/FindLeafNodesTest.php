<?php
/**
 * Find leaf nodes test
 *
 * @package Slacademic
 * @subpackage Tests
 * @author Eric
 */
namespace Slacademic\Tests\Questions;

class FindLeafNodesTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Find leaf nodes
     */
    public function testFindLeafNodes()
    {
        $leafer = new \Slacademic\Questions\FindLeafNodes();

        $leaves = $leafer->findLeafNodesOfPreorder([5, 3, 2, 4, 8, 7, 9]);

        $this->assertEquals([2, 4, 7, 9], $leaves);
    }
}