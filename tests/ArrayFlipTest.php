<?php
/**
 * Tests
 *
 * @package Slacademic
 * @subpackage Tests
 * @uathor Eric
 */
namespace Slacademic\Tests\Questions;

class ArrayFlipTest extends \PHPUnit\Framework\TestCase
{
    public function testQuestion()
    {
        $this->assertTrue(\Slacademic\Questions\ArrayFlip::isSameFlipped([1, 6, 0, 9, 1]));
        $this->assertFalse(\Slacademic\Questions\ArrayFlip::isSameFlipped([1, 7, 1]));
    }
}