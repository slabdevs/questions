<?php
/**
 * Tests
 *
 * @package Slacademic
 * @subpackage Tests
 * @uathor Eric
 */
namespace Slacademic\Tests\Questions;

class StringPossibilitiesTest extends \PHPUnit\Framework\TestCase
{
    public function testQuestion()
    {
        $map = [
            '1' => ['A', 'B', 'C'],
            '2' => ['D', 'E'],
            '12' => ['X'],
            '3' => ['P', 'Q']
        ];

        $result = \Slacademic\Questions\StringPossibilities::patterns($map, '123');

        $expected = [
            'ADP', 'ADQ', 'AEP', 'AEQ', 'BDP', 'BDQ', 'BEP', 'BEQ', 'CDP', 'CDQ', 'CEP', 'CEQ', 'XP', 'XQ'
        ];

        $this->assertEquals($expected, $result);
    }
}