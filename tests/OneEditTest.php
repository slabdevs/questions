<?php
/**
 * One edit tests
 *
 * @package Slacademic
 * @subpackage Tests
 * @uathor Eric
 */
namespace Slacademic\Tests\Questions;

class OneEditTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Test question
     */
    public function testQuestion()
    {
        $this->assertTrue(\Slacademic\Questions\OneEdit::oneEditAway('from', 'fom'));
        $this->assertTrue(\Slacademic\Questions\OneEdit::oneEditAway('a', 'b'));
        $this->assertTrue(\Slacademic\Questions\OneEdit::oneEditAway('', ''));
        $this->assertTrue(\Slacademic\Questions\OneEdit::oneEditAway('x', ''));
        $this->assertTrue(\Slacademic\Questions\OneEdit::oneEditAway('from', 'rom'));
        $this->assertTrue(\Slacademic\Questions\OneEdit::oneEditAway('thing', 'thifng'));
        $this->assertFalse(\Slacademic\Questions\OneEdit::oneEditAway('thing', 'fng'));
        $this->assertFalse(\Slacademic\Questions\OneEdit::oneEditAway('asdf', 'asdfasdfasdfsadf'));
    }
}
