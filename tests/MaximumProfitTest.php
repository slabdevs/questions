<?php
/**
 * Test for maximum profit question
 *
 * @package Slacademic
 * @subpackage Tests
 * @author Eric
 */
namespace Slacademic\Tests\Questions;

class MaximumProfitTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Test maximum profits
     */
    public function testMaximumProfits()
    {
        $profits = new \Slacademic\Questions\MaximumProfit();

        $value = $profits->calculateMaximumProfit([19, 22, 15, 35, 40, 10, 20]);

        $this->assertEquals(25, $value);
    }
}