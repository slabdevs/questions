<?php
/**
 * Tests for TV Room
 *
 * @package Slacademic
 * @subpackage Tests
 * @author Eric
 */
namespace Slacademic\Tests\Questions;

class TVRoomTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Test TV room timings
     *
     * @dataProvider dataProviderTVRoomTestings
     */
    public function testTVRoomTimings($array, $value)
    {
        $tvRoom = new \Slacademic\Questions\TVRoom();

        $computed = $tvRoom->getTimeCoveredByIntervals($array);

        $this->assertEquals($value, $computed);
    }

    /**
     * Data provider for tv room testings
     *
     * @return array
     */
    public function dataProviderTVRoomTestings()
    {
        return [
            [[[1,4],[2,3]], 3],
            [[[4,6],[1,2]], 3],
            [[[1,4],[6,8],[2,4],[7,9],[10,15]], 11]
        ];
    }
}