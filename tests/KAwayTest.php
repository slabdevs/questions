<?php
/**
 * Tests
 *
 * @package Slacademic
 * @subpackage Tests
 * @uathor Eric
 */
namespace Slacademic\Tests\Questions;

class KAwayTest extends \PHPUnit\Framework\TestCase
{
    public function testQuestion()
    {
        $node1 = new \Slacademic\Questions\KAway(21);
        $node2 = new \Slacademic\Questions\KAway(14);
        $node3 = new \Slacademic\Questions\KAway(33);
        $node4 = new \Slacademic\Questions\KAway(77);
        $node5 = new \Slacademic\Questions\KAway(144);
        $node6 = new \Slacademic\Questions\KAway(13);

        $node1->next =& $node2;
        $node2->next =& $node3;
        $node3->next =& $node4;
        $node4->next =& $node5;
        $node5->next =& $node6;

        $this->assertEquals(21, \Slacademic\Questions\KAway::getKthFromEnd($node1, 6));
        $this->assertEquals(14, \Slacademic\Questions\KAway::getKthFromEnd($node1, 5));
        $this->assertEquals(33, \Slacademic\Questions\KAway::getKthFromEnd($node1, 4));
        $this->assertEquals(77, \Slacademic\Questions\KAway::getKthFromEnd($node1, 3));
        $this->assertEquals(144, \Slacademic\Questions\KAway::getKthFromEnd($node1, 2));
        $this->assertEquals(13, \Slacademic\Questions\KAway::getKthFromEnd($node1, 1));
    }
}