<?php
/**
 * # There's a room with a TV and people are coming in and out to watch it. The TV is on only when there's at least a person in the room.
    # For each person that comes in, we record the start and end time. We want to know for how long the TV has been on. In other words:
    # Given a list of arrays of time intervals, write a function that calculates the total amount of time covered by the intervals.
    # For example:

    # input = [(1,4), (2,3)]
    # > 3
    # input = [(4,6), (1,2)]
    # > 3
    # input = {{1,4}, {6,8}, {2,4}, {7,9}, {10, 15}}
    # > 11
 *
 * @package Slacademic
 * @subpackage Questions
 * @author Eric
 * @see https://www.careercup.com/question?id=5765433895419904
 */
namespace Slacademic\Questions;

class TVRoom
{
    /**
     * Build hashmap representation of the clock, then sum the hashmap
     *
     * @param $input
     * @return int
     */
    public function getTimeCoveredByIntervals($input)
    {
        $clock = [];

        foreach ($input as $timeRange)
        {
            for ($j=$timeRange[0];$j<$timeRange[1];++$j)
            {
                $clock[$j] = 1;
            }
        }

        return array_sum($clock);
    }
}