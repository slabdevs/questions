<?php
/**
 * Given an array of integers, design an algorithm that moves all non-zero integers to
 * the end of the array. Minimize the number of writes or swaps.
 *
 * @see https://www.careercup.com/question?id=5734664078622720
 *
 * @package Slacademic
 * @subpackage Questions
 * @author Eric
 *
 */
namespace Slacademic\Questions;

class MoveZeros
{
    /**
     * Move zeroes to end of an array
     *
     * @param $array
     */
    public function moveZerosToEnd(&$array)
    {
        $totalItems = count($array);
        $insertPoint = $totalItems - 1;

        for ($i=0; $i<$totalItems; ++$i)
        {
            if ($array[$i] == 0)
            {
                while ($array[$insertPoint] == 0)
                {
                    $insertPoint--;
                    if ($insertPoint <= $i) return;
                }

                $this->swap($array, $i, $insertPoint);
            }
        }
    }

    /**
     * Swap params
     *
     * @param $array
     * @param $i
     * @param $j
     */
    private function swap(&$array, $i, $j)
    {
        $tmp = $array[$i];
        $array[$i] = $array[$j];
        $array[$j] = $tmp;
    }
}