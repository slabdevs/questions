<?php
/**
 * Question 1: Given an input of an array of string, verify if, turned 180 degrees, it is the "same".
For instance:
[1, 6, 0, 9, 1] => return true
[1, 7, 1] => return false
 *
 * @see https://www.careercup.com/question?id=5757912092770304
 */
namespace Slacademic\Questions;

class ArrayFlip
{
    static public function isSameFlipped($numbers)
    {
        $matches = [
            1 => 1,
            6 => 9,
            9 => 6,
            0 => 0,
            8 => 8
        ];

        $count = count($numbers);
        for ($i=0,$j=($count - 1); $i<$count; $i++, $j--)
        {
            $currentLetter = $numbers[$i];
            $flipped = $numbers[$j];

            if (!isset($matches[$currentLetter]) || $flipped !== $matches[$currentLetter])
            {
                return false;
            }
        }

        return true;
    }
}