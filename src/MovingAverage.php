<?php
/**
 * @see https://www.careercup.com/question?id=6313643925831680
 */
namespace Slacademic\Questions;

class MovingAverage
{
    private $maxSize = 0;

    private $sum = 0;

    private $numbers = [];

    private $count = 0;

    public function __construct($n)
    {
        $this->maxSize = $n;
    }

    public function pushNumber($number)
    {
        $this->sum += $number;
        $this->numbers[] = $number;

        $this->count = count($this->numbers);

        if ($this->count > $this->maxSize)
        {
            $val = array_shift($this->numbers);
            $this->sum -= $val;
            $this->count -= 1;
        }
    }

    public function average()
    {
        if ($this->count == 0) return 0;

        return ($this->sum / $this->count);
    }
}