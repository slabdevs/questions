<?php
/**
 * @see https://en.wikipedia.org/wiki/Look-and-say_sequence
 *
 */
namespace Slacademic\Questions;

class LookAndSay
{
    static public function las($n)
    {
        if ($n < 1) return;

        $last = '';
        for ($i = 1; $i <= $n; ++$i)
        {
            $current = '';
            if ($i == 1)
            {
                $current = '1';
            }
            else
            {
                $charCount = strlen($last);
                $context = '';
                $count = '';
                for ($j = 0; $j < $charCount; ++$j)
                {
                    if (empty($context))
                    {
                        $context = $last[$j];
                        $count = 1;
                        continue;
                    }

                    if (($last[$j] != $context))
                    {
                        $current .= $count . $context;
                        $context = $last[$j];
                        $count = 1;
                    } else {
                        $count++;
                    }
                }

                $current .= $count . $context;
            }
            echo $current . PHP_EOL;
            $last = $current;
        }
    }
}