<?php

namespace Slacademic\Questions;

/**
 * Given an array of N numbers, find three that add up to zero
 */
class ThreeItemsZeroSum
{
    public static function execute($array)
    {
        $matches = [];
        $count = count($array);
        for ($x = 0; $x < $count - 2; ++$x) {
            for ($y = $x + 1; $y < $count - 1; ++$y) {
                for ($z = $y + 1; $z < $count; ++$z) {
                    if ($array[$x] + $array[$y] + $array[$z] === 0) {
                        $matches[] = [$array[$x], $array[$y], $array[$z]];
                    }
                }
            }
        }
        return $matches;
    }

    public static function execute2($array)
    {
        // [0, -1, 2, -3, 1]
        $c = count($array);
        $values = [];
        $triples = [];
        for ($x = 0; $x < $c - 1; ++$x) {
            for ($y = $x + 1; $y < $c; ++$y) {
                $value = ($array[$x] + $array[$y]) * -1;
                if (isset($values[$value])) {
                    $triples[] = [$value, $array[$x], $array[$y]];
                } else {
                    $values[$array[$y]] = true;
                }
            }
        }
        return $triples;
    }
}