<?php
/**
 * Convert an integer to an english representation
 *
 * I got my interview yesterday and the problem they asked me was: Giving a method intToEnglish that
 * receives an int as a parameter, how do you return its representation in english words. The number can be
 * of any size but no more than around 2 billion since the parameter is an int 2ˆ32
 *
 * @package Slacademic
 * @subpackage Questions
 * @author Eric
 * @see https://www.careercup.com/question?id=5177754057179136
 */
namespace Slacademic\Questions;

class IntToEnglish
{
    /**
     * Return english represenation of an integer
     *
     * @param $integer
     * @return string
     */
    public function englishRepresentationOfInteger($integer)
    {
        $output = '';

        while ($integer)
        {
            $output .= $this->getLargestPortionOfNumber($integer) . ' ';
        }

        return trim($output);
    }

    /**
     * Get the largest portion of a number
     *
     * @param $number
     */
    private function getLargestPortionOfNumber(&$number)
    {
        $divisors = [
            1000000000 => 'Billion',
            1000000 => 'Million',
            1000 => 'Thousand',
            100 => 'Hundred',
            10 => 'Tens'
        ];

        $textuals = [
            9 => 'Nine',
            8 => 'Eight',
            7 => 'Seven',
            6 => 'Six',
            5 => 'Five',
            4 => 'Four',
            3 => 'Three',
            2 => 'Two',
            1 => 'One',
            0 => 'Zero',
            90 => 'Ninety',
            80 => 'Eighty',
            70 => 'Seventy',
            60 => 'Sixty',
            50 => 'Fifty',
            40 => 'Fourty',
            30 => 'Thirty',
            20 => 'Twenty',
            11 => 'Eleven',
            12 => 'Twelve',
            13 => 'Thirteen',
            14 => 'Fourteen',
            15 => 'Fifteen',
            16 => 'Sixteen',
            17 => 'Seventeen',
            18 => 'Eighteen',
            19 => 'Nineteen'
        ];

        foreach ($divisors as $divisor => $units)
        {
            if ($number < $divisor) continue;

            $original = $number;
            $value = floor($number / $divisor);
            $number = $number % $divisor;

            if ($units == 'Tens')
            {
                //In this case we have a "Three Hundred Seventeen" and we dont want "Three Hundred One Ten Seven"
                if (!empty($textuals[$original]))
                {
                    $number = 0;
                    return $textuals[$original];
                }

                //Otherwise get the *10 representation in text
                return $textuals[$value * 10];
            }

            if (!empty($textuals[$value]))
            {
                //We have a direct mapping here
                return $textuals[$value] . ' ' . $units;
            }
            else
            {
                //Deal with spaces between the divisors, fill in recursively for like "320 Million"
                if ($value < 999 && $value >= 10)
                {
                    $pre = '';
                    $subNumber = $value;
                    while ($subNumber)
                    {
                        $pre .= $this->getLargestPortionOfNumber($subNumber) . ' ';
                    }
                    return $pre . $units;
                }
                else
                {
                    return $value . ' ' . $units;
                }
            }
        }

        $output = $textuals[$number];
        $number = 0;

        return $output;
    }

}