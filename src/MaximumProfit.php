<?php
/**
 * Given estimated stock quotes, in an array, print the maximum profit from a buy and sell
 *
 * @package Slacademic
 * @subpackage Questions
 * @author Eric
 * @see https://www.careercup.com/question?id=5661553962516480
 */
namespace Slacademic\Questions;

class MaximumProfit
{
    /**
     * Calculate the maximum profit from a buy and then sale in an array
     *
     * @param $values
     */
    public function calculateMaximumProfit($values)
    {
        $profit = 0;
        $minimum = PHP_INT_MAX;

        $count = count($values);
        for ($i=0;$i<$count;++$i)
        {
            $minimum = min($minimum, $values[$i]);
            $profit = max($profit, ($values[$i] - $minimum));
        }

        return $profit;
    }
}