<?php
/**
 * Return the value of item k away from the end of a linked list
 *
 * @package Slacademic
 * @subpackage Questions
 * @author Eric
 * @see https://www.careercup.com/question?id=5204502847160320
 */
namespace Slacademic\Questions;

class KAway
{
    public $val;

    public $next;

    public function __construct($val)
    {
        $this->val = $val;
    }

    static public function getKthFromEnd(KAway $root, $k)
    {
        $slow =& $root;
        $fast =& $root;

        for ($i=0;$i<$k;++$i)
        {
            $fast =& $fast->next;
        }

        while (!empty($fast))
        {
            $fast =& $fast->next;
            $slow =& $slow->next;
        }

        return $slow->val;
    }
}