<?php
/**
 * @see https://en.wikipedia.org/wiki/Look-and-say_sequence
 *
 */
namespace Slacademic\Questions;

class LinkedListCycle
{
    public $val;

    public $next;

    public function __construct($x)
    {
        $this->val = $x;
        $this->next = null;
    }

    static public function findCycle(LinkedListCycle $node)
    {
        $hash = [];
        $pointer =& $node;
        while (!empty($pointer))
        {
            if (!empty($hash[$pointer->val]))
            {
                return $pointer;
            }

            $hash[$pointer->val] = true;
            $pointer = $pointer->next;
        }

        return null;
    }
}