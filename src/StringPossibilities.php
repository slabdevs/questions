<?php
/**
 *
 * @see https://www.careercup.com/question?id=5634222671790080
 */
namespace Slacademic\Questions;

class StringPossibilities
{
    /**
     * '1' : ['A', 'B', 'C']
     * '2' : ['D', 'E']
     * '12' : ['X']
     * '3' : ['P', 'Q']
     *
     * @param $map
     * @param $string
     * @return array
     */
    public static function patterns($map, $string)
    {
        $response = [];

        if (empty($string)) return $response;
        if (empty($map)) return $response;

        if (!empty($map[$string])) return $map[$string];

        foreach ($map as $key => $items)
        {
            if (substr($string, 0, strlen($key)) == $key)
            {
                $remaining = substr($string, strlen($key));

                foreach ($items as $char)
                {
                    $ends = static::patterns($map, $remaining);

                    foreach ($ends as $end)
                    {
                        $response[] = $char . $end;
                    }
                }
            }
        }

        return $response;
    }
}