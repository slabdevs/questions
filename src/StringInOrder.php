<?php
/**
 * @see https://www.careercup.com/question?id=5179225018466304
 */
namespace Slacademic\Questions;

class StringInOrder
{
    static public function inOrder($words, $ordering)
    {
        $orderIndex = 0;
        $started = false;
        foreach ($words as $word)
        {
            $len = strlen($word);
            for ($i=0;$i<$len;++$i)
            {
                $char = $word[$i];

                if ($char == $ordering[$orderIndex])
                {
                    $started = true;
                    continue;
                }

                if (!$started) continue;

                $orderIndex++;
                if (empty($ordering[$orderIndex])) return true;

                if ($char != $ordering[$orderIndex])
                {
                    return false;
                }
            }
        }

        return true;
    }
}