<?php
/**
 * String decoder problem
 *
 * 3[a2[bd]g4[ef]h]
 *
 * @see https://www.careercup.com/question?id=5692396030394368
 * @package Slacademic
 * @subpackage Questions
 * @author Eric
 */
namespace Slacademic\Questions;

class StringDecoder
{
    /**
     * Return decoded string
     *
     * @param $string
     */
    public function decode($string)
    {
        $done = false;
        $currentPosition = 0;

        $multiplier = 0;

        $output = '';

        while (!$done && !empty($string[$currentPosition]))
        {
            if (is_numeric($string[$currentPosition]))
            {
                $multiplier = $string[$currentPosition];
            }
            else if ($string[$currentPosition] == '[')
            {
                $subPosition = $currentPosition + 1;
                $endingPosition = null;
                $braces = 0;
                while (empty($endingPosition))
                {
                    if ($string[$subPosition] == ']')
                    {
                        if ($braces == 0)
                        {
                            $endingPosition = $subPosition;
                        }
                        else
                        {
                            $braces--;
                        }
                    }
                    else if ($string[$subPosition] == '[')
                    {
                        $braces++;
                    }
                    $subPosition++;
                }

                $subString = substr($string, $currentPosition + 1, $endingPosition - $currentPosition - 1);

                $subStringData = $this->decode($subString);

                if (!empty($multiplier))
                {
                    for ($j=0;$j<$multiplier;++$j)
                    {
                        $output .= $subStringData;
                    }
                }
                else
                {
                    $output .= $subStringData;
                }

                $currentPosition = $endingPosition;
            }
            else
            {
                $output .= $string[$currentPosition];
            }

            $currentPosition++;
        }

        return $output;
    }
}
