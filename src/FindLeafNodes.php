<?php
/**
 * Find leaf nodes
 * Given a list of nodes find the leaf nodes without building the tree
 *
 * @package Slacademic
 * @subpackage Questions
 * @author Eric
 * @see https://www.careercup.com/question?id=5749516234915840
 */
namespace Slacademic\Questions;

class FindLeafNodes
{
    /**
     * Find the leaf nodes
     *
     * For preorder traversal we can determine if its a leaf if there is a decrease from the previous
     * and an increase from the current.
     *
     * @param $nodes
     */
    public function findLeafNodesOfPreorder($nodes)
    {
        if (empty($nodes)) return [];

        $numberOfNodes = count($nodes);

        if ($numberOfNodes == 1) return $nodes;
        if ($numberOfNodes == 2) return $nodes[1];

        $leaves = [];
        $stack = [$nodes[0]];

        for ($i=1; $i<$numberOfNodes; ++$i)
        {
            if ($nodes[$i-1] > $nodes[$i])
            {
                $stack[] = $nodes[$i];
            }
            else
            {
                $found = false;
                while (!empty($stack))
                {
                    if ($stack[count($stack)-1] < $nodes[$i])
                    {
                        array_pop($stack);
                        $found = true;
                    }
                    else
                        break;
                }
                if ($found)
                    $leaves[] = $nodes[$i-1];
            }
        }
        $leaves[] = $nodes[count($nodes)-1];

        return $leaves;
    }
}