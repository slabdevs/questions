<?php
/**
 * Palindrome check
 */
namespace Slacademic\Questions;

class Palindrome
{
    static public function isPalindrome($string)
    {
        $length = strlen($string);

        if ($length < 2) return false;

        $index = 0;
        $reverse = $length - 1;
        do
        {
            while (!static::isValid($string[$index]))
            {
                $index++;
            }

            while (!static::isValid($string[$reverse]))
            {
                $reverse--;
            }

            if ($index >= $length || $reverse < 0)
            {
                return false;
            }

            if ($string[$index] != $string[$reverse])
            {
                return false;
            }

            $index++;
            $reverse--;
        }
        while ($index < $length);

        return true;
    }

    static public function isValid($char)
    {
        return ($char >= 'a' && $char <= 'z');
    }
}