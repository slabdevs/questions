<?php
/**
 * Print an NxM matrix with NW-SE Diagonals starting at the bottom left corner
 *
 * @package Slacademic
 * @subpackage Questions
 * @author Eric
 */
namespace Slacademic\Questions;

class NwSeMatrix
{
    /**
     * Print a matrix
     *
     * @param $matrix
     */
    public function printMatrix($matrix)
    {
        $matrixHeight = count($matrix);
        $matrixWidth = count($matrix[0]);

        $overrun = $matrixHeight - 1;

        for ($x=0;$x<$matrixWidth + $overrun;++$x)
        {
            for ($y=($matrixHeight-1);$y>=0;$y--)
            {
                if (!empty($matrix[$matrixHeight - $y - 1][$x - $y]))
                {
                    echo $matrix[$matrixHeight - $y - 1][$x - $y] . ' ';
                }
            }
            echo "\n";
        }
    }
}