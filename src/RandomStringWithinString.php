<?php
/**
 * Given a random string S and another string T with unique elements, find the minimum consecutive sub-string of S such that it contains all the elements in T.
 * example:
 * S='adobecodebanc'
 * T='abc'
 * answer='banc'
 *
 * @package Slacademic
 * @subpackage Questions
 * @author Eric
 * @see https://www.careercup.com/question?id=4855286160424960
 */
namespace Slacademic\Questions;

class RandomStringWithinString
{
    /**
     * Get minimum sub string
     *
     * @param $s
     * @param $t
     */
    public function getMinimumSubString($s, $t)
    {
        $stringLength = strlen($s);

        $start = 0;
        $end = strlen($t);
        $bestStart = 0;
        $bestEnd = PHP_INT_MAX;

        $patternHistogram = $this->makeHistogram($t);

        $stretching = true;

        while ($start < $stringLength)
        {
            $substring = substr($s, $start, $end);

            $histogram = $this->makeHistogram($substring);

            $substringHasString = $this->histogramContainsHistogram($histogram, $patternHistogram);

            if ($substringHasString)
            {
                if (($end - $start) < ($bestEnd - $bestStart))
                {
                    $bestStart = $start;
                    $bestEnd = $end;
                }
            }

            if ($stretching)
            {
                if ($end == $stringLength)
                {
                    $stretching = false;
                    $start++;
                }
                else
                {
                    $end++;
                }
            }
            else
            {
                $start++;
            }
        }


        return substr($s, $bestStart, $bestStart + $bestEnd);
    }

    /**
     * Make histogram of a string
     *
     * @param $string
     * @return array
     */
    private function makeHistogram($string)
    {
        $histogram = [];
        for ($i = 0; $i < strlen($string); ++$i)
        {
            $histogram[$string[$i]] = 1;
        }

        return $histogram;
    }

    /**
     * Histogram contains a histogram
     *
     * @param $challenge
     * @param $pattern
     * @return bool
     */
    private function histogramContainsHistogram($challenge, $pattern)
    {
        foreach ($pattern as $key => $value)
        {
            if (empty($challenge[$key])) return false;
        }

        return true;
    }
}