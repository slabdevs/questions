<?php
/**
 *  Write a function to return if two words are exactly "one edit" away, where an edit is:
    Inserting one character anywhere in the word (including at the beginning and end)
    Removing one character
    Replacing exactly one character
 */
namespace Slacademic\Questions;

class OneEdit
{
    /**
     * @param $string1
     * @param $string2
     * @return bool
     */
    static public function oneEditAway($string1, $string2)
    {
        $length1 = mb_strlen($string1);
        $length2 = mb_strlen($string2);

        if (abs($length1 - $length2) > 2)
        {
            return false;
        }

        if ($length1 < 2)
        {
            return true;
        }

        $changes = 0;
        $done = false;

        $index1 = $index2 = 0;

        while (!$done)
        {
            $char1 = $string1[$index1];
            $char2 = $string2[$index2];

            if ($char1 != $char2)
            {
                if (!isset($string2[$index2 + 1]) || $string2[$index2 + 1] == $char1)
                {
                    $changes++;
                    $index2++;
                }
                else if (!isset($string1[$index1 + 1]) || $string1[$index1 + 1] == $char2)
                {
                    $changes++;
                    $index1++;
                }
                else
                {
                    $changes++;
                    $index1++;
                    $index2++;
                }
            }
            else
            {
                $index1++;
                $index2++;
            }

            if (!isset($string1[$index1]) || !isset($string2[$index2]) || $changes > 1)
            {
                $done = true;
            }
        }

        return $changes <= 1;
    }
}