<?php
namespace Slacademic\Questions;

class Spiral
{
    static public function spiral($n)
    {
        $array = array_fill(0, $n, []);
        for ($j = 0; $j < $n; ++$j)
        {
            $array[$j] = array_fill(0, $n, 0);
        }

        $total = $n * $n;
        $x = $y = 0;
        $xdir = 1;
        $ydir = 0;

        for ($i=0;$i<$total;++$i)
        {
            $array[$y][$x] = $i + 1;

            if (!isset($array[$y + $ydir][$x + $xdir]) || ($array[$y + $ydir][$x + $xdir] != 0))
            {
                if ($xdir == 1 && $ydir == 0)
                {
                    $xdir = 0;
                    $ydir = 1;
                }
                else if ($xdir == 0 && $ydir == 1)
                {
                    $xdir = -1;
                    $ydir = 0;
                }
                else if ($xdir == -1 && $ydir == 0)
                {
                    $xdir = 0;
                    $ydir = -1;
                }
                else
                {
                    $xdir = 1;
                    $ydir = 0;
                }
            }

            $x += $xdir;
            $y += $ydir;
        }

        return $array;
    }
}